const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')
const path = require('path');
const fs = require('fs')
const multer = require('multer');
const upload = multer({dest: 'uploads/'});


const app = express()
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
)
app.use(express.static('uploads'))
app.use(bodyParser.json())

app.use(cors())
const port = 3000

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.post('/upload-image', upload.single('file'), (req, res) => {
  if (!req.file) {
    console.log("No file received");
    return res.send({
      success: false,
      filename: ''
    });

  } else {
    console.log('file received');
    return res.send({
      success: true,
      filename: `http://localhost:${port}/${req.file.filename}`
    })
  }
});

app.post('/download', (req, res) => {
  fs.readFile('index.html', 'utf8' , (err, data) => {
    if (err) {
      console.error(err)
      return
    }
    let dd = data.split('<body>')
    res.send(dd[0] + '<body>' + req.body.htmlStr + dd[1]);
  })
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})